# Merging
## Combines the work of multiple branches

* Fast-foward merge
    - Moves the base branch label to the tip of the topic branch
    - Possible if no other commits have been made to the base branch since branching

* Merge commit
    - Combines the commits at the tips of the merged branches
    - Places the result in the merge commit
    - A merge commit has multiple parents
    
* Rebase

    - Moves commits to a new parent (base)
    -> Ancestor chain is different, each of the reapplied commits has a different commit ID
    - Rewrite history of branch with interactive rebase (edit, drop, pick, squash)
    - Pros:
        * You can incorporate changes from the parent branch
            - You can use the new features/bugfixes
            - Tests are on more current code
            - It makes the eventual merge into master fast-fowardable
        * Avoids "unnecessary" commits
            - It allows you to shape/define clean commit histories
    - Cons:
        * Merge conflicts may need to be resolved
        * It can cause problems if your commits have * been shared
        * You are not preserving the commit history

    - Usage
    ```git
        git rebase <upstream> <branch>
        e.g. git rebase master develop          
    ``` 
    `upstream` usually refers the parent branch of the rebased branch

    - Squash commit (not merge)
        - Combined this commit with the older commit, creating a single commit with `git rebase -i \<after-this-commit>`
        - The work of both commit is included

* Squash merge
    - Merges the tip of the feature branch onto the tip of the base branch
    - Places the result in the staging arena 
    - The result can then be committed.
    - The squash merge rewrites the commit history
    - Can squash merge with fast foward


    


