# Git References
### User-friendly name that points to:
* A commit SHA-1 hash
* Another reference
    * known as a symbolic reference

## Branch Label
* Points to the most recent commit in the branch (tip of the branch)
* Implemented as a reference

### HEAD 
* A reference to the current commit
* Usually points to the branch label of the current branch (e.g. HEAD -> master)
* One HEAD per repo

## Tags
* Reference/label attached to a specific commit (user-friendly label for the commit)

Types of tags
* Lightweight

    - A simple reference to a commit

* Annotated

    - A full git object that references a commit
    - Includes tag author info, tag date, tag message, the commit ID
    - Optionally can be signed and verified with GNU Privacy Guard


## Usage

Refers to a prior commit
* ~ or ~1 = parent
* ~2 or ~~ = parent's parent

Refers to a parent in a merge commit (^parentnum)
* ^ or ^1 = first parent of the commit
* ^2 = second parent of a merge commit
* ^^ = first parent's first parent

