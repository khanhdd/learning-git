# Branches
## A branch is a set of commits that trace back to the project's first commit. 
## Creating a branch creates a branch label

* Topic 
    - A feature, bug fix, hotfix, a configuration change, etc...
* Long-lived
    - Master, develop, release,...

## Checkout

* Update the HEAD reference
* Update the working tree with the commit's files

### Deatached HEAD (Checkout a commit instead branch)
* Temporary view an older version of the project
* HEAD points directly to the SHA-1 of a commit (instead of branch label)
* Fixing a deatached HEAD (proper way)
    - Create a branch on that commit
    - Checkout that branch (HEAD -> new branch label)

## Deleting a branch 
* Commonly delete after a topic branch has been merged
* Undo an accidental branch delete with `git reflog`

## Tracking Branch
### A local branch that represents a remote branch
* \<remote>/\<branch> (e.g. origin/master)

### Just another branch, like topic branch


